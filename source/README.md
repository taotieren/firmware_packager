# firmware_packager

### 介绍
这是一个 bin 固件打包器， 为 bin 固件附上一个 fpk 表头（[《fpk固件包表头信息》](https://gitee.com/DinoHaw/firmware_packager/blob/master/document/fpk%E5%9B%BA%E4%BB%B6%E5%8C%85%E8%A1%A8%E5%A4%B4%E4%BF%A1%E6%81%AF.pdf)），该打包器是 [mOTA](https://gitee.com/DinoHaw/mOTA) 组件的一部分。

### 实现的功能
#### 固件打包部分
1.  
![fpk固件打包器](https://gitee.com/DinoHaw/mOTA/blob/master/image/fpk固件打包器.png)

#### 固件解析部分
![fpk固件打包器_解析](https://gitee.com/DinoHaw/mOTA/blob/master/image/fpk固件打包器_解析.png)



#### 编译环境

1.  本工程采用 Qt6 编译， Qt5 也支持，需要自己设置一下（如果不知道怎么编译，直接用 [`.exe`]() 文件即可）
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
